import { IsNotEmpty, MinLength, IsPositive } from 'class-validator';
export class CreateCustomerDto {
  @IsNotEmpty()
  name: string;

  @IsNotEmpty()
  @IsPositive()
  age: number;

  @IsNotEmpty()
  @MinLength(10)
  tel: string;

  @IsNotEmpty()
  gender: string;
}
